
.SILENT:
# export DOCKER_HEALTHCHECK_TEST=/bin/true
compose_cmd = docker compose -p rails_project

help:
	echo "up build stop reload restart exec run update"

up: build
	$(compose_cmd) up -d

build:
	docker buildx build ./docker/

stop:
	$(compose_cmd) stop

reload: stop up

restart:
	$(compose_cmd) restart web

launch: up
	$(compose_cmd) exec web fish

run:
	$(compose_cmd) run --rm web fish

docker: .SHELLFLAGS = run -v $(shell pwd):/app -v ../rails:/rails --rm --workdir /app --entrypoint /bin/bash web -c
docker: SHELL := docker
docker:
	-git clone https://github.com/rails/rails.git /rails 2> /dev/null
	pushd /rails && git pull && bundle config set --local without rubocop mdl doc job cable storage ujs test db && bundle update --bundler && bundle install && popd
	rm -rf app/ bin/ config/ db/ lib/ public/ storage/ test/ vendor/ config.ru Gemfile.lock Rakefile README.md
	../rails/railties/exe/rails new . --database=postgresql --skip-bootsnap --skip-bundle --asset-pipeline=propshaft --javascript=esbuild --css=tailwind --skip-jbuilder --main --edge --force

update:
	sudo chown -R $(USER): {,.}*
	-git clone https://github.com/rails/rails.git ../rails 2> /dev/null
	pushd ../rails && git pull && bundle config set --local without rubocop mdl doc job cable storage ujs test db && bundle update --bundler && bundle install && popd
	rm -rf app/ bin/ config/ db/ lib/ public/ storage/ test/ vendor/ config.ru Gemfile.lock Rakefile README.md
	../rails/railties/exe/rails new . --database=postgresql --skip-bootsnap --skip-bundle --asset-pipeline=propshaft --javascript=esbuild --css=tailwind --skip-jbuilder --main --edge --force
	sudo chown -R $(USER): {,.}*
